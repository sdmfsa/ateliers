---
title: "Optimización"
author: "carlos.chaparro@fsa.ulaval.ca"
date: "`r format(Sys.time(), '%d %B, %Y')`"
---

## Introducción
<small><a href="https://cran.r-project.org/other-docs.html" target="_blank"><i class="fa fa-book-open"></i></a> Manuales <br> <a href="https://cran.r-project.org/web/views/Optimization.html" target="_blank"><i class="fas fa-list-ol"></i></a> Paquetes R </small>


### Objetivos

En este taller vamos a aprender a :

+ Crear una función con `R`.
+ Visualizar una función.
+ Encontrar las raíces de una función.
+ Encontrar los puntos mínimos/máximos de una función de una variable.
+ Encontrar los puntos mínimos/máximos de una función de varias variables.

## Funciones
Antes de hablar de optimización, vamos a comenzar por crear una función con `R`.

### Función objetivo

\begin{align}
f(r, \mathbf{x}) = \sum_{i=1}^N \frac{x_i}{(1+r)^i}
\end{align}

Donde $r \in \mathbb{R}$ y $\mathbf{x} = \left( x_1, x_2, \dots, x_N \right), x_i \in \mathbb{R}$.

```{r vanfun, exercise=TRUE}
mi_funcion <- function(r, x) {
  # Empezar aquí
}

```

```{r vanfun-solution}
mi_funcion <- function(r, x) {
  n_max <- length(x)
  factores <- 1/((1+r)^(1:n_max))

  return(sum(factores*x))
}

mi_funcion(r=0.05, x=c(100, 110, 120, 130, 1000))
```

### Gráfico

Después de haber creado nuestra función, podemos visualizarla creando un gráfico con la función `plot`. Para esto, vamos a tomar $100$ puntos para la variable $r \in \left[0, 1\right]$ y los siguientes valores para la variable $x$:

```{r, eval=FALSE}
set.seed(20)

x1 <- rnorm(1, -1000, 100)
x2 <- rnorm(1, 1000, 100)
x3 <- rnorm(1, 1000, 100)
x4 <- rnorm(1, 1000, 200)
x5 <- rnorm(1, -2000, 200)

x <- c(x1,x2,x3,x4,x5)  # Este comando crea la variable x
```

```{r vanchart, exercise=TRUE}
mi_funcion <- function(r, x) {
  n_max <- length(x)
  factores <- 1/((1+r)^(1:n_max))

  return(sum(factores*x))
}

set.seed(20)

x1 <- rnorm(1, -1000, 100)
x2 <- rnorm(1, 1000, 100)
x3 <- rnorm(1, 1000, 100)
x4 <- rnorm(1, 1000, 200)
x5 <- rnorm(1, -2000, 200)

x <- c(x1,x2,x3,x4,x5)

## Empezar aquí

r_vals <- seq(0, 1, length.out=100)
```

```{r vanchart-solution}
mi_funcion <- function(r, x) {
  n_max <- length(x)
  factores <- 1/((1+r)^(1:n_max))

  return(sum(factores*x))
}

set.seed(20)

x1 <- rnorm(1, -1000, 100)
x2 <- rnorm(1, 1000, 100)
x3 <- rnorm(1, 1000, 100)
x4 <- rnorm(1, 1000, 200)
x5 <- rnorm(1, -2000, 200)

x <- c(x1,x2,x3,x4,x5)

## Empezar aquí

r_vals <- seq(0, 1, length.out=100)

f_vals <- numeric(length(r_vals))

for (i in 1:length(r_vals))  {
    f_vals[i] <- mi_funcion(r_vals[i], x)
}

plot(x=r_vals, y=f_vals, type="l", xlab="r (%)", ylab="VAN ($)")
```

## Raíces

Ahora que sabemos cómo crear y cómo visualizar una función con `R`, intentemos encontrar sus raíces, es decir los valores de $r, f(r) = 0$. Note que a partir de ahora consideraremos la variable `x` como una constante. Entonces, la función de interés es :

\begin{align}
f(r \vert \mathbf{x}) = \sum_{i=1}^N \frac{x_i}{(1+r)^i}
\end{align}

Donde $r\in \mathbb{R}$ y $\mathbf{x}$ es un vector que toma los valores del ejemplo anterior.

+ La función `uniroot` nos permitirá encontrar las raíces de una función de una variable.
+ La función `abline` nos permitirá agregar líneas horizontales o verticales a un gráfico creado con `plot`.

> Para obtener ayuda sobre la utilización de una función, hay que agregar el símbolo `?` al momento de ejecutar el comando. Por ejemplo, ejecutando el comando `?uniroot` obtendremos la página de ayuda de la función `uniroot`.

```{r prepare-vans}
mi_funcion <- function(r, x) {
  n_max <- length(x)
  factores <- 1/((1+r)^(1:n_max))

  return(sum(factores*x))
}

set.seed(20)

x1 <- rnorm(1, -1000, 100)
x2 <- rnorm(1, 1000, 100)
x3 <- rnorm(1, 1000, 100)
x4 <- rnorm(1, 1000, 200)
x5 <- rnorm(1, -2000, 200)

x <- c(x1,x2,x3,x4,x5)
```

```{r irruniroot, exercise=TRUE, exercise.setup="prepare-vans"}
r_vals <- seq(0, 5, 0.01)
y_vals <- sapply(r_vals, mi_funcion, x=x)

plot(x=r_vals, y=y_vals, type="l", xlab="r", ylab="f(r)")
abline(h = 0.0, lty = 2)  # Agrega una línea horizontal en y = h
```

```{r irruniroot-solution}
r_vals <- seq(0, 5, 0.01)
y_vals <- sapply(r_vals, mi_funcion, x = x)

plot(x=r_vals, y=y_vals, type="l", xlab="r", ylab="f(r)")
abline(h = 0.0, lty = 2)  # Agrega una línea horizontal en y = h

(racine_1 <- uniroot(mi_funcion, x = x, lower = 0, upper = 0.5))
abline(v=racine_1$root, col=2)

(racine_2 <- uniroot(mi_funcion, x = x, lower = 0.5, upper = 2))
abline(v=racine_2$root, col=3)

```

## Una variable

Ya conocemos cuales son las raíces de la función, pero dónde se encuentra su máximo global ? Y su mínimo ?

+ La función `optimize` nos ayuda a encontrar los puntos máximos (mínimos) de una función de una variable.

```{r vanoptimize, exercise=TRUE, exercise.setup="prepare-vans"}
r_vals <- seq(0, 5, 0.01)
y_vals <- sapply(r_vals, mi_funcion, x = x)

plot(x=r_vals, y=y_vals, type="l", xlab="r", ylab="f(r)")
abline(h=0.0, lty=2)  # Agrega una línea horizontal en y = h

# Continuar aquí
```

```{r vanoptimize-solution}
r_vals <- seq(0, 5, 0.01)
y_vals <- sapply(r_vals, mi_funcion, x = x)

plot(x=r_vals, y=y_vals, type="l", xlab="r", ylab="f(r)")
abline(h=0.0, lty=2)  # Agrega una línea horizontal en y = h

(f_max <- optimize(mi_funcion, x = x, lower=0, upper=1, maximum=TRUE))
abline(h=f_max$objective, col=3, lty=3)

(f_min <- optimize(mi_funcion, x = x, lower=1, upper=5, maximum=FALSE))
abline(h=f_min$objective, col=2, lty=3)
```

## Varias variables

Pero, cómo encontramos el mínimo(máximo) de una función de varias variables ? Para esto, tomemos como ejemplo la siguiente función :

\begin{align}
f(x, y) = \left(a-x\right)^2 + b\left(y-x^2\right)^2
\end{align}

Donde $a, b$ son constantes. Vamos a utilizar los valores $a=1$ y $b=100$, luego encontraremos el mínimo global de esta función con ayuda de `R`.

> Esta función se llama la función de Rosenbrock <a href="https://es.qaz.wiki/wiki/Rosenbrock_function#Optimization_examples" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> y su mínimo global se encuentra en el punto $(a, a^2)$.

```{r , echo=FALSE, eval=TRUE}
banana_fun <- function(parvec, a = 1, b = 100)  {
  # parvec es un vector de dos elementos
  x <- parvec[1]
  y <- parvec[2]

  return(((a-x)^2) + b*((y-x^2)^2))
}

axex <- seq(-2,2,0.1)
axey <- seq(-1,3,0.1)

gridvals <- expand.grid(axex, axey)
axez <- apply(gridvals, 1, banana_fun)

axez <- matrix(axez, ncol=length(axex), byrow=TRUE)

p <- plotly::plot_ly(x=axex, y=axey, z=axez) %>%
      add_surface(
        contours=list(
          z = list(
          show=TRUE,
          usecolormap=TRUE,
          highlightcolor="#ff0000",
          project=list(z=TRUE)
          )
        )
      ) %>%
      layout(
        title="f(x,y)",
        paper_bgcolor = "transparent",
        plot_bgcolor = "transparent"
      )
p
```

+ La función `optim` nos permite encontrar el mínimo de una función de varias variables.

> Ejecute el comando `?optim` para acceder a los detalles <a href="https://stat.ethz.ch/R-manual/R-devel/library/stats/html/optim.html" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> sobre la utilización de esta función.

```{r bananamin, exercise=TRUE}
obj_fun <- function(parvec, a=1, b=100)  {
  # parvec es un vector de dos elementos
  x <- parvec[1]
  y <- parvec[2]

  return(((a-x)^2) + b*((y-x^2)^2))
}
```

```{r bananamin-solution}
obj_fun <- function(parvec, a=1, b=100)  {
  # parvec es un vector de dos elementos
  x <- parvec[1]
  y <- parvec[2]

  return(((a-x)^2) + b*((y-x^2)^2))
}

init_pars <- runif(2, -1, 1)  # 2 números aleatorios distribuidos uniformemente

out <- optim(par=init_pars, obj_fun)
out  # mínimo en el punto (a, a^2)
```

## Ejercicio

+ Escoja una función de la lista de funciones test para optimización <a href="https://en.wikipedia.org/wiki/Test_functions_for_optimization" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> y encuentre el mínimo(máximo) global con la función `optim`.


```{r test-optim, exercise=TRUE}
obj_fun <- function(parvec) {
  # Empiece por crear la función
  return(0)
}

init_pars <- 0 # Punto de inicio para la búsqueda

out <- optim(par=init_pars, obj_fun)
out
```

---
title: "Régression linéaire et optimisation"
author: "Carlos C.S. <cachs1@ulaval.ca>"
date: "`r format(Sys.time(), '%Y-%m-%d')`"
description: "Quelques exemples d'optimisation avec R."
output:
  learnr::tutorial:
    progressive: true
    allow_skip: true
    language: fr
    css: css/styles.css
    include:
      after_body: footer.html
runtime: shiny_prerendered
---

```{r setup, include=FALSE}
library(learnr)
library(plotly)
library(ISLR2)
attach(Boston)

tutorial_options(exercise.timelimit = 10)
```

## Introduction
<small><a href="https://www.statlearning.com" target="_blank"><i class="fa fa-book-open"></i></a> Livre <br> <a href="https://www.statlearning.com/resources-second-edition" target="_blank"><i class="fas fa-file-download"></i></a> Fichiers </small>


### Objectifs

Au cours de cet atelier, nous allons :

+ Obtenir une prévision pour une variable d'intérêt $Y$ à partir d'une matrice $X = (X_1, X_2, \dots, X_p)$.
+ Créer une fonction $Y = f(X)$.
+ Visualiser cette fonction.
+ Trouver le maximum/minimum d'une fonction $Y = f(X)$.

## Régression

> Le tableau `Boston` vient avec le paquetage `R` accompagnant le livre [An Introduction to Statistical Learning](https://www.statlearning.com/).

+ Affichez les premiers $6$ lignes du tableau `Boston`.

```{r reg0, exercise=TRUE}

```

```{r reg0-solution}
library(ISLR2)  # install.packages("ISLR2") si pas encore installé
head(Boston)
```

### Fonction lm

La fonction `lm` nous permet de faire une régression linéaire à une ou a multiples variables, incluant des effets d'interaction. La commande `lm(y ~ x, data = mon_tableau)` permet de faire la régression $y = \beta_0 + \beta_1 x_1$ où les variables `y` et `x` correspondent à $y$ et $x_1$ dans l'équation ci-dessous.

+ Faites la régression linéaire suivante et sauvegardez l'output dans une variable nommée `out_lm`:

\begin{align}
  y &= \beta_0 + \beta_1 x_1 \hfill \text{, où}  \\
  y &= \texttt{medv} \\
  x_1 &= \texttt{lstat}
\end{align}

> Les détails sur chaque colonne sont accessibles avec la commande `?Boston`.

```{r reg1, exercise=TRUE}

```

```{r reg1-solution}
out_lm <- lm(medv ~ lstat, data = Boston)
```

### Notation matricielle

Nous pouvons arriver aux mêmes valeurs pour $\hat{\beta_0}$ et $\hat{\beta_1}$ obtenus ci-haut à l'aide de l'expression mathématique suivante :

\begin{align}
\hat{\beta} &=  (\mathbf{X}^\intercal \mathbf{X})^{-1} \mathbf{X}^\intercal \mathbf{Y}
\end{align}

+ Faites la même régression linéaire à l'aide de l'équation ci-haut.


```{r regmat, exercise=TRUE}
X <- cbind(1, Boston$lstat)
Y <- Boston$medv
```

```{r regmat-solution}
X <- cbind(1, Boston$lstat)
Y <- Boston$medv

out <- solve(t(X)%*%X)%*%t(X)%*%Y
out_lm <- lm(medv ~ lstat, data = Boston)

out
out_lm$coefficients  # Ou coef(out_lm)
```

### Statistiques

Nous pouvons obtenir un sommaire des statistiques associées aux hypothèses souvent testées dans une analyse de régression linéaire <a href="https://online.stat.psu.edu/stat501/lesson/6/6.1" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> à l'aide de la commande `summary`.

+ Trouvez les statistiques $t$ associées à $\hat{\beta_0}$ et à $\hat{\beta_1}$.
+ Trouvez la statistique $F$ de la régression.
+ Trouvez un intervalle de confiance au seuil de $95\%$ pour les paramètres estimés.


```{r reg2, exercise=TRUE}
out_lm <- lm(medv ~ lstat, data = Boston)
```

```{r reg2-solution}
out_stats <- summary(out_lm)
out_stats$fstatistic  # Voir names(out_summary)
confint(out_lm)
```

### Prévision

Au moment d'obtenir une prévision pour $Y$ correspondant à une valeur de $X$, on s'intéresse également aux intervalles autour de notre prévision pour quantifier l'incertitude autour de $y$. L'intervalle autour d'une prédiction $y$ est plus large que celui autour de $\text{E} (Y | X)$ <a href="https://online.stat.psu.edu/stat501/lesson/3/3.3" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>.

+ Trouvez les *intervalles de confiance* pour une prévision de `medv`. Utilisez les valeurs $5, 10$ et $15$ pour `lstat`.
+ Trouvez les *intervalles de prédiction* pour une prévision de `medv`. Utilisez les valeurs $5, 10$ et $15$ pour `lstat`.

```{r reg3, exercise=TRUE}
out_lm <- lm(medv ~ lstat, data = Boston)
```

```{r reg3-solution}
predict(out_lm, data.frame(lstat = (c(5, 10, 15))), interval = "confidence")
predict(out_lm, data.frame(lstat = (c(5, 10, 15))), interval = "prediction")
```

> Notez que la seule différence entre les deux outputs se trouve au niveau des colonnes `lwr` et `upr`.

### Graphique

+ Présentez les couples de points $(x_i, y_i)$ sur un graphique à $2$ dimensions.
+ Ajoutez au graphique la droite de régression linéaire.


```{r reg4, exercise=TRUE}
out_lm <- lm(medv ~ lstat, data = Boston)
```

```{r reg4-solution}
plot(lstat, medv)  # attach(Boston)
abline(out_lm)
```

+ Présentez sur une même feuille les $4$ graphiques obtenus en appliquant la fonction `plot` à l'output de la régression.


```{r reg5, exercise=TRUE}
out_lm <- lm(medv ~ lstat, data = Boston)
```

```{r reg5-solution}
par(mfrow = c(2, 2))
plot(out_lm)
```

+ Comparez plusieurs lignes de régression sur le même graphique.

```{r reg6, exercise=TRUE}
out_1 <- lm(medv ~ lstat, data = Boston)
out_2 <- lm(medv ~ lstat + I(lstat^2), data = Boston)  # lstat + lstat^2
out_3 <- lm(medv ~ lstat + I(lstat^2) + + I(lstat^3), data = Boston)
#out_4 <- lm(medv ~ poly(lstat, 3), data = Boston)  # Équivalent à out_3
```

```{r reg6-solution}
# attach(Boston)  # Pour dire lstat au lieu de Boston$lstat
plot(lstat, medv)
lines(sort(lstat), predict(out_1)[order(lstat)], col = 1)
lines(sort(lstat), predict(out_2)[order(lstat)], col = 2)
lines(sort(lstat), predict(out_3)[order(lstat)], col = 3)
#lines(sort(lstat), predict(out_4)[order(lstat)], col = 4)
```

+ Comparez l'output de deux régressions.

```{r reg7, exercise=TRUE}
out_1 <- lm(medv ~ lstat, data = Boston)
out_poly <- lm(medv ~ lstat + I(lstat^2), data = Boston)  # lstat + lstat^2
#out_inter <- lm(medv ~ lstat * age, data = Boston)  # lstat + age + lstat:age
```

```{r reg7-solution}
anova(out_1, out_poly)
plot(lstat, medv)
abline(out_1)
abline(out_poly)
```

## Fonctions

### Fonction objectif

\begin{align}
f(r, \mathbf{x}) = \sum_{i=1}^N \frac{x_i}{(1+r)^i}
\end{align}

Où $r \in \mathbb{R}$ et $\mathbf{x} = \left( x_1, x_2, \dots, x_N \right), x_i \in \mathbb{R}$.

```{r vanfun, exercise=TRUE}
ma_fonction <- function(r, x) {
  # Commencer ici
}

```

```{r vanfun-solution}
ma_fonction <- function(r, x) {
  grand_n <- length(x)
  facteurs <- 1/((1+r)^(1:grand_n))

  return(sum(facteurs*x))
}

ma_fonction(r=0.05, x=c(100, 110, 120, 130, 1000))
```

### Graphique

Ensuite, nous pouvons visualiser cette fonction en créant un graphique à l'aide de la fonction `plot`. Prenons par exemple $100$ points pour la variable $r \in \left[0, 1\right]$ ainsi que les les valeurs suivantes pour la variable $x$:

```{r, eval=FALSE}
set.seed(20)

x1 <- rnorm(1, -1000, 100)
x2 <- rnorm(1, 1000, 100)
x3 <- rnorm(1, 1000, 100)
x4 <- rnorm(1, 1000, 200)
x5 <- rnorm(1, -2000, 200)

x <- c(x1,x2,x3,x4,x5)
```

```{r vanchart, exercise=TRUE}
ma_fonction <- function(r, x) {
  grand_n <- length(x)
  facteurs <- 1/((1+r)^(1:grand_n))

  return(sum(facteurs*x))
}

set.seed(20)

x1 <- rnorm(1, -1000, 100)
x2 <- rnorm(1, 1000, 100)
x3 <- rnorm(1, 1000, 100)
x4 <- rnorm(1, 1000, 200)
x5 <- rnorm(1, -2000, 200)

x <- c(x1,x2,x3,x4,x5)

## Commencer ici

r_vals <- seq(0, 1, length.out=100)
```

```{r vanchart-solution}
ma_fonction <- function(r, x) {
  grand_n <- length(x)
  facteurs <- 1/((1+r)^(1:grand_n))

  return(sum(facteurs*x))
}

set.seed(20)

x1 <- rnorm(1, -1000, 100)
x2 <- rnorm(1, 1000, 100)
x3 <- rnorm(1, 1000, 100)
x4 <- rnorm(1, 1000, 200)
x5 <- rnorm(1, -2000, 200)

x <- c(x1,x2,x3,x4,x5)

## Commencer ici

r_vals <- seq(0, 1, length.out=100)

f_vals <- numeric(length(r_vals))

for (i in 1:length(r_vals))  {
    f_vals[i] <- ma_fonction(r_vals[i], x)
}

plot(x=r_vals, y=f_vals, type="l", xlab="r (%)", ylab="VAN ($)")
```

## Optimisation

Nous avons connaissons déjà les racines de la fonction, mais où se trouve le maximum ? Et le minimum ?

### Une variable

+ La fonction `optimize` nous permet de trouver le maximum(minimum) d'une fonction à une variable.

```{r prepare-vans}
ma_fonction <- function(r, x) {
  grand_n <- length(x)
  facteurs <- 1/((1+r)^(1:grand_n))

  return(sum(facteurs*x))
}

set.seed(20)

x1 <- rnorm(1, -1000, 100)
x2 <- rnorm(1, 1000, 100)
x3 <- rnorm(1, 1000, 100)
x4 <- rnorm(1, 1000, 200)
x5 <- rnorm(1, -2000, 200)

x <- c(x1,x2,x3,x4,x5)
```

```{r vanoptimize, exercise=TRUE, exercise.setup="prepare-vans"}
r_vals <- seq(0, 5, 0.01)
y_vals <- sapply(r_vals, ma_fonction, x = x)

plot(x=r_vals, y=y_vals, type="l", xlab="r", ylab="f(r)")
abline(h=0.0, lty=2)  # Ajoute ligne horizontale à x = h

# Continuer ici
```

```{r vanoptimize-solution}
r_vals <- seq(0, 5, 0.01)
y_vals <- sapply(r_vals, ma_fonction, x = x)

plot(x=r_vals, y=y_vals, type="l", xlab="r", ylab="f(r)")
abline(h=0.0, lty=2)  # Ajoute ligne horizontale à x = h

(f_max <- optimize(ma_fonction, x = x, lower=0, upper=1, maximum=TRUE))
abline(h=f_max$objective, col=3, lty=3)

(f_min <- optimize(ma_fonction, x = x, lower=1, upper=5, maximum=FALSE))
abline(h=f_min$objective, col=2, lty=3)
```

### Plusieurs variables

Mais comment trouver le minimum/maximum d'une fonction à plusieurs variables ? Pour répondre à cette question, nous allons prendre comme exemple la fonction suivante :

\begin{align}
f(x, y) = \left(a-x\right)^2 + b\left(y-x^2\right)^2
\end{align}

Où $a, b$ sont des constantes. Nous allons utiliser les valeurs $a=1$ et $b=100$ pour ensuite trouver le minimum global de cette fonction numériquement à l'aide de `R`.

> Cette fonction s'appelle la fonction de Rosenbrock <a href="https://en.wikipedia.org/wiki/Rosenbrock_function" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> et son minimum global se trouve au point $(a, a^2)$.

```{r , echo=FALSE, eval=TRUE}
banana_fun <- function(parvec, a = 1, b = 100)  {
  # parvec est un vecteur de taille 2 avec les paramètres de la fonction
  x <- parvec[1]
  y <- parvec[2]

  return(((a-x)^2) + b*((y-x^2)^2))
}

axex <- seq(-2,2,0.1)
axey <- seq(-1,3,0.1)

gridvals <- expand.grid(axex, axey)
axez <- apply(gridvals, 1, banana_fun)

axez <- matrix(axez, ncol=length(axex), byrow=TRUE)

p <- plotly::plot_ly(x=axex, y=axey, z=axez) %>%
      add_surface(
        contours=list(
          z = list(
          show=TRUE,
          usecolormap=TRUE,
          highlightcolor="#ff0000",
          project=list(z=TRUE)
          )
        )
      ) %>%
      layout(
        title="f(x,y)",
        paper_bgcolor = "transparent",
        plot_bgcolor = "transparent"
      )
p
```

+ La fonction `optim` nous permet de trouver le minimum d'une fonction à plusieurs variables.

> Faites la commande `?optim` pour accéder aux détails <a href="https://stat.ethz.ch/R-manual/R-devel/library/stats/html/optim.html" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> sur l'utilisation de cette fonction.

```{r bananamin, exercise=TRUE}
obj_fun <- function(parvec, a=1, b=100)  {
  # parvec est un vecteur de taille 2 avec les paramètres de la fonction
  x <- parvec[1]
  y <- parvec[2]

  return(((a-x)^2) + b*((y-x^2)^2))
}
```

```{r bananamin-solution}
obj_fun <- function(parvec, a=1, b=100)  {
  # parvec est un vecteur de taille 2 avec les paramètres de la fonction
  x <- parvec[1]
  y <- parvec[2]

  return(((a-x)^2) + b*((y-x^2)^2))
}

init_pars <- runif(2, -1, 1)  # 2 tirages aléatoires, loi uniforme

out <- optim(par=init_pars, obj_fun)
out  # minimum à (a, a^2)
```

### Exercice

+ Choisissez une fonction de la liste de fonctions test pour optimisation <a href="https://en.wikipedia.org/wiki/Test_functions_for_optimization" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> et trouvez le minimum global à l'aide de la fonction `optim`.


```{r test-optim, exercise=TRUE}
obj_fun <- function(parvec) {
  # Commencer par créer la fonction
  return(0)
}

init_pars <- 0 # Point de départ pour la recherche

out <- optim(par=init_pars, obj_fun)
out  # minimum
```

